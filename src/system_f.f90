module system
  USE iso_c_binding

  INTERFACE
    SUBROUTINE mkdir(filename) BIND(C,name="fortran_mkdir")
      USE iso_c_binding
      CHARACTER(kind=C_CHAR),INTENT(in) :: filename(*)
    END SUBROUTINE

    SUBROUTINE gzip(filename,output) BIND(C,name="gzip")
      USE iso_c_binding
      CHARACTER(kind=C_CHAR),INTENT(in) :: filename(*)
      CHARACTER(kind=C_CHAR),INTENT(in) :: output(*)
    END SUBROUTINE

    SUBROUTINE zcat(filename,output) BIND(C,name="zcat")
      USE iso_c_binding
      CHARACTER(kind=C_CHAR),INTENT(in) :: filename(*)
      CHARACTER(kind=C_CHAR),INTENT(in) :: output(*)
    END SUBROUTINE

  END INTERFACE
end module

